#!/usr/bin/env bash
#
# Download Polarstern location data for a specified time range
#
# Usage: getpstrack.sh Tstart [Tend]
#
# Tstart and Tend are in UTC and must be formatted as:
#
#    YYYY-mm-ddTHH:MM:SS
#
# Tend defaults to now. Output goes to standard out.
#

PARENT="vessel:polarstern:hydrins_1"
BASEURL="https://dashboard.awi.de/data-xxl/rest/data"
QUERY=(
    "format=text/tab-separated-values"
    "aggregate=hour"
    "aggregateFunctions=MEAN"
    "sensors=${PARENT}:heading"
    "sensors=${PARENT}:latitude"
    "sensors=${PARENT}:longitude"
)

tstart="$1"
[[ -z "$tstart" ]] && {
    echo "Usage: $(basename $0) Tstart [Tend]" 1>&2
    exit 1
}
tend="$2"
[[ -z "$tend" ]] && tend="$(date -u +'%FT%T')"

q="$(printf '%s&' ${QUERY[@]})beginDate=${tstart}&endDate=${tend}"

curl -X GET "${BASEURL}?${q}"

#!/usr/bin/env bash
#
# Generate a KML file from a set of GPS files from a DOT buoy.
#
export PATH=$HOME/bin:$PATH

listing="/tmp/genkml.list.$$"
trap "rm -f $listing" 0 1 2 3 15

[[ -f .env ]] && source  .env

# Dump a sorted list of all GPS data files newer than $REF_TIME to
# a file.
find . -newermt "$REF_TIME" -name 'pvt*.sbf' -print |\
    sort > "$listing"

# Create the KML file
gpstokml --output track.kml "@${listing}"

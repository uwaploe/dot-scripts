#!/usr/bin/env bash
#
#
export PATH=$HOME/bin:$PATH

PARENT="vessel:polarstern:hydrins_1"
BASEURL="https://dashboard.awi.de/data-xxl/rest/data"
QUERY=(
    "format=text/tab-separated-values"
    "aggregate=hour"
    "aggregateFunctions=MEAN"
    "sensors=${PARENT}:heading"
    "sensors=${PARENT}:latitude"
    "sensors=${PARENT}:longitude"
)

# We need GNU date
if which gdate 1> /dev/null 2>&1; then
    DATE=gdate
else
    DATE=date
fi

datafile="$1"
[[ -z "$datafile" ]] && {
    echo "Usage: $(basename $0) posfile" 2>&1
    exit 1
}

if [[ -s "$datafile" ]]; then
    # Add 1 hour to the timestamp of the final record in the file
    val=$($DATE -u -d "$(tail -n 1 $datafile | cut -f 1)" +%s)
    tstart="$($DATE -u -d @$((val + 3600)) +%FT%T)"
else
    tstart="$2"
    [[ -n $tstart ]] || {
        echo "Start date/time required: YYYY-mm-dd[THH:MM:SS]"
        exit 1
    }
fi

tend="$(date -u +'%FT%T')"
q="$(printf '%s&' ${QUERY[@]})beginDate=${tstart}&endDate=${tend}"
cp "$datafile" "${datafile}.orig"
curl -X GET "${BASEURL}?${q}" | sed -n -e '2,$p' >> "$datafile"

tsvtokml --color "#ffffff00" --title 'Polarstern track' --output pstrack.kml "$datafile"

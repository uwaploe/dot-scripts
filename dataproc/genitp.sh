#!/usr/bin/env bash
#
# Generate a KML track file for the ITP buoy (113) deployed on the same
# floe as DOT-1
#
PATH=$HOME/bin:$PATH

set -o errexit

: ${ITPURL="ftp://ftp.whoi.edu/whoinet/itpdata/itp113rawlocs.dat"}

declare -r tmpfile="$(mktemp)"
trap "rm -f $tmpfile" EXIT

curl -s "$ITPURL" > $tmpfile
itptokml --color "#ffffff00" --title 'ITP Buoy 113' --output itp.kml "$tmpfile"

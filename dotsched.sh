#!/usr/bin/env bash
#
# Create a DOT system update package which will start a new schedule from
# the supplied satellite overpass file and optionally transfer the package
# file to a server for later downloading by the DOT controller.
#

: ${DOT_SERVER=128.95.97.223}
: ${DOT_SERVER_ACCT=mobilesys}

# The postinst script will enable the required timers and
# run dotsleep to start the schedule.
write_postinst () {
    cat<<EOF
#!/usr/bin/env bash

systemctl enable sleepcheck.timer
systemctl enable summary-upload.timer
systemctl start dotsleep.service

EOF

}

# Preinst will check that dotsleep is installed
write_preinst () {
    cat<<"EOF"
#!/usr/bin/env bash

[ -x "$(command -v dotsleep)" ]
EOF
}

outfile="newsched.upd"
doupload=
schedopts=()
identity=
usage="Usage: $(basename $0) [-t max_sleep][-u] dot_id overpass_file"
while getopts "humso:i:t:" opt; do
    case $opt in
        o)
            outfile="$OPTARG"
            ;;
        t)
            schedopts+=("--tmax=$OPTARG")
            ;;
        m)
            schedopts+=("--merge")
            ;;
        s)
            schedopts+=("--sat")
            ;;
        u)
            doupload=1
            ;;
        i)
            identity="-i $OPTARG"
            ;;
        h)
            echo "$usage" 1>&2
            exit 0
            ;;
    esac
done

shift $(($OPTIND - 1))
if [[ $# -lt 2 ]]; then
    echo "Missing command-line argument" 1>&2
    echo "$usage" 1>&2
    exit 2
fi
dot_id="$1"

# Create package contents in a temporary directory.
d="$(mktemp -d)"
pwd="$PWD"
cd $d
trap "rm -rf $d" INT TERM EXIT
mkdir CONTROL
write_postinst > CONTROL/postinst
chmod a+x CONTROL/postinst
write_preinst > CONTROL/preinst
chmod a+x CONTROL/preinst
mkdir -p usr/local/etc
mksched "${schedopts[@]}" < "$pwd/$2" > usr/local/etc/dotsched.yaml || {
    echo "Cannot create schedule file"
    exit 1
}
cd "$pwd"

# Create the package file
 if createupd --unsigned --root "/"  -o "$outfile" "$d"; then
     # Upload it
     if [[ -n $doupload ]]; then
         sftp -o StrictHostKeyChecking=no $identity -b - $DOT_SERVER_ACCT@$DOT_SERVER <<EOF
cd $dot_id/OUTBOX
put $outfile
exit
EOF
     fi
 fi

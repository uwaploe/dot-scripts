# Plot DOT buoy attitude sensor data
reset

# Exponential moving-average filter. An alpha value of 1 provides
# approximately the same smoothing as an averaging window of 199
#
# N = (2/alpha) - 1
#
alpha = 0.01
state = 0
ema(i,x)=i > 1 ? (state=alpha*x + (1. - alpha)*state, state) : (state=x, state)

set datafile separator ","
set key autotitle columnhead
set xdata time
set timefmt '%Y-%m-%d %H:%M:%S'
set xtics 864000
set xlabel 'Year-day/Time UTC'
set format x "%j/%H:%M"
set ylabel 'Degrees Magnetic'
set y2label 'Degrees'
set ytics nomirror
set y2tics
set title "DOT Buoy: `printenv DOT_DEPLOYMENT`\nAttitude Sensor"
set key left bottom Left box

plot 'angles.csv' using 1:2 w l axes x1y1 title 'Compass', \
     '' using 1:(ema($0,$2)) w l axes x1y1 title 'Smoothed', \
     '' using 1:3 w l axes x1y2 title 'Pitch', \
     '' using 1:4 w l axes x1y2 title 'Roll'

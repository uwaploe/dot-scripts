# Plot DOT buoy pressure time series
reset

# Exponential moving-average filter. An alpha value of 1 provides
# approximately the same smoothing as an averaging window of 199
#
# N = (2/alpha) - 1
#
alpha = 0.01
state = 0
ema(i,x)=i > 1 ? (state=alpha*x + (1. - alpha)*state, state) : (state=x, state)

set datafile separator ","
set xdata time
set timefmt '%Y-%m-%d %H:%M:%S'
# Convert psi to meters SW (dbar)
meters(x)=x*0.68947573
set ylabel 'Dbars'
set yrange [*:*] reverse
set xlabel 'Year-day/Time UTC'
set format x "%j/%H:%M"
set xtics 864000
set title "DOT Buoy: `printenv DOT_DEPLOYMENT`\nPressure Sensor Depth"
set grid
set nokey

plot 'pr.csv' using 1:(meters($2)) axes x1y1 w l title 'depth', \
     '' using 1:(ema($0,meters($2))) axes x1y1 w l title 'smoothed'

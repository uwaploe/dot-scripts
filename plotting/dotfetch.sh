#!/usr/bin/env bash
#
# Download the most recent DOT Buoy CSV data of a specific measurement
# type. Valid types are; pr, eng, env, and angles. Data records are appended to
# a file named "$measurement".csv in the current directory. If no file
# exists, the user must specify the start time on the command line,
# otherwise the start time is read from the last record of the file.
#
# Environment variables:
#
#    DOT_ID - DOT buoy ID (e.x. dot-1)
#    DOT_HOST - SFTP server IP address and port
#    DOT_SSHKEY - Path to SSH private key used to access the server
#
# If a file named ".env" is found in the current directory, it is sourced
# to obtained env variable settings.

[[ -z "$1" ]] && {
    echo  "Usage: $(basename  $0) measurement [t_start]" 1>&2
    exit 1
}

if which gdate 1> /dev/null 2>&1; then
    DATE=gdate
else
    DATE=date
fi

: ${DOT_HOST=10.95.97.213:22}

[[ -f .env ]] && source .env

meas="$1"
datafile="${meas}.csv"
if [[ -s "$datafile" ]]; then
    t_start=$($DATE -u -d "$(tail -n 1 $datafile | cut -f 1 -d,)" +%FT%T)
else
    t_start="$2"
    [[ -n $t_start ]] || {
        echo "Start date/time required: YYYY-mm-dd[THH:MM:SS]"
        exit 1
    }
fi

drop=()
if [[ $meas = "angles" ]]; then
    drop=("--drop" "accelx,accely,accelz,gyrox,gyroy,gyroz")
fi

cp "$datafile" "${datafile}.orig"
tsdata get --tstart "$t_start" \
       --tags "id=$DOT_ID" "${drop[@]}" \
       $DOT_DB_NAME "$meas" | sed -n -e '2,$p' >> "$datafile"

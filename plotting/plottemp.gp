# Plot DOT buoy temperature time series from a CSV file.
reset
set encoding utf8
set datafile separator ","
set key autotitle columnhead
set xdata time
set timefmt '%Y-%m-%d %H:%M:%S'
set xtics 864000
set xlabel 'Year-day/Time UTC'
set format x "%j/%H:%M"
set ylabel '°C'
set grid
set title "DOT Buoy: `printenv DOT_DEPLOYMENT`\nTemperature"
set key left bottom Left box

plot 'metavg.csv' using 1:2 w l title 'External', \
     '' using 1:4 w l title 'Internal'

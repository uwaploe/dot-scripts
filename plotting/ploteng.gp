# Plot DOT buoy battery voltage time series
reset
set datafile separator ","
set key autotitle columnhead
set xdata time
set timefmt '%Y-%m-%d %H:%M:%S'
set xtics 172800
set xlabel 'Year-day/Time UTC'
set format x "%j/%H:%M"
set ylabel 'Volts'
set title "DOT Buoy: `printenv DOT_DEPLOYMENT`\nBattery Voltages"
set key left bottom Left box

plot 'eng.csv' using 1:2 w l title 'V_{batt}', \
     '' using 1:5 w l title 'V_{solar}', \
     '' using 1:3 w l title 'V_{aux}', \
     '' using 1:4 w l title 'V_{pri}'

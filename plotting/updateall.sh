#!/usr/bin/env bash

types=("metavg" "pr" "tcm")
for type in "${types[@]}"; do
    dotfetch.sh $type
done


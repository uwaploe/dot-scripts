# DOT Shore Server Scripts

This repository contains various scripts used for managing and processing data from the DOT Buoys. See the [Wiki](https://bitbucket.org/uwaploe/dot-scripts/wiki/) for documentation of the server setup and data handling procedure.

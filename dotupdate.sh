#!/usr/bin/env bash
#
# Create an update (*.upd) package for the DOT Buoy. The structure is almost
# identical to an IPKG package file. Some of the code in this script was "borrowed"
# from ipkg-build.
#
# Example package layout:
#
# <pkgdir>/
# ├── CONTROL/
# │   ├── postinst*
# │   └── preinst*
# └── etc/
#     └── systemd/
#         └── system/
#             ├── gnssdaq.service
#             ├── pppd@.service
#             ├── startgnss.service
#             ├── startgnss.timer
#             ├── stopgnss.service
#             └── stopgnss.timer
#
# The CONTROL subdirectory contains two shell scripts; preinst is executed
# before any new files are installed and postinst is executed after. Any
# other files/directories under <pkgdir> will be installed as-is under the
# root filesystem of the buoy controller.

usage="Usage: $(basename $0) directory pkgname"

if [[ $# -lt 2 ]]; then
    echo $usage 1>&2
    exit 1
fi

# We need GNU tar
if which gtar 1> /dev/null 2>&1; then
    TAR=gtar
else
    TAR=tar
fi

pkgdir="$1"
pkgname="$PWD/$2"

if [[ ! -d "$pkgdir" ]]; then
    echo "ERROR: Package directory \"$pkgdir\" not found" 1>&2
    exit 1
fi

if [[ ! -d "$pkgdir/CONTROL" ]]; then
    echo "ERROR: Package directory has no CONTROL subdirectory" 1>&2
    exit 1
fi

[[ -f "$pkgdir/CONTROL/preinst" ]] && chmod a+x "$pkgdir/CONTROL/preinst"
[[ -f "$pkgdir/CONTROL/postinst" ]] && chmod a+x "$pkgdir/CONTROL/postinst"

tmpdir="$(mktemp -d)"
trap "rm -rf $tmpdir" 0 1 2 3 15

set -e

(cd "$pkgdir" && $TAR --owner=0 --group=0 --exclude=CONTROL -czf $tmpdir/files.tar.gz .)
(cd "$pkgdir/CONTROL" && $TAR --owner=0 --group=0 -czf $tmpdir/control.tar.gz .)
(cd $tmpdir && $TAR --owner=0 --group=0 -czf "$pkgname" ./files.tar.gz ./control.tar.gz)

echo "Packaged contents of \"$pkgdir\" into \"$pkgname\""
